<?php
/**
 * Simple app to find journeys via london underground
 *
 * Not entirely fast, but it appears to work
 */

require_once 'Include/functions.php';

$from = 'Kentish Town';
$to = 'South Wimbledon';
$route = [];

//$map = createMap('Data/NorthernAndVictoria.csv');  // produces 6 valid routes with the 26 station limit
$map = createMap('Data/AllRoutes.csv');      // produces 1355 valid routes with the 26 station limit

// to remove the 50 minute restriction, remove the "26" maxStations parameter from the following call
$routes = findRoute($map, $from, $to, $route, 26);

// remove duplicate routes, if any
$routes = array_map('unserialize', array_unique(array_map('serialize', $routes)));
var_dump($routes);

exit();
