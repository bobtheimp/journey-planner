<?php
/**
 * include file for all main functions
 */

if (!function_exists('createMap')) {
    /**
     * @param string $filename
     * @return array
     */
    function createMap(string $filename): array
    {
        $handle = fopen($filename, 'rb');
        if (!is_resource($handle)) {
            exit ("Unable to open '$filename' for reading!".PHP_EOL);
        }

        $map = [];

        while (!feof($handle)) {
            $line = fgetcsv($handle, 4096);
            list($stationName, $neighbour) = $line;

            // build the graph map
            if (!array_key_exists($stationName, $map)) {
                $map[$stationName] = [];
            }
            $map[$stationName][] = $neighbour;
            if (!array_key_exists($neighbour, $map)) {
                $map[$neighbour] = [];
            }
            $map[$neighbour][] = $stationName;

            // to calculate the time better, we can add time data to the map array and calculate it
            // when we add stations to the route
        }

        fclose($handle);

        ksort($map);

        return $map;
    }
}

if (!function_exists('findRoute')) {
    /**
     * @param array $map
     * @param string $start
     * @param string $end
     * @param array $route
     * @param int|null $maxStations
     * @return array|array[]
     */
    function findRoute(array $map, string $start, string $end, array $route, int $maxStations = null): array
    {
        $route[$start] = count($route);

        if (null !== $maxStations && count($route) > $maxStations) {
            return [];
        }

        if ($start === $end) {
            return [$route];
        }

        if (!array_key_exists($start, $map)) {
            return [];
        }

        $routes = [];

        foreach ($map[$start] as $index => $station) {
            if (!array_key_exists($station, $route)) {
                $newRoutes = findRoute($map, $station, $end, $route, $maxStations);
                foreach ($newRoutes as $newRoute) {
                    $routes[] = $newRoute;
                }
            }
        }

        return $routes;
    }
}