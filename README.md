# Journey Planner #

This example is with vanilla PHP7.4 

### How do I get set up? ###

To run:

- Clone the repository
- Execute php ./main.php from the base directory

### Notes and improvements ###

Perhaps pass a paremeter in to determine the map to use. Currently it defaults to "AllRoutes.csv".
Adding an interface would also be quote handy.